import filter from './reducer/filter.js'
import posts from './reducer/posts.js'

export function reducer(state = {}, action = {}) {
  return {
    filter: filter(state.filter, action),
    posts: posts(state.posts, action)
  }
}
