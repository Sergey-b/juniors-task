export default function filter(oldPosts = [], action) {
  switch (action.type) {
    case 'ADD_POSTS':
      return oldPosts.concat(action.payload)
    case 'REMOVE_POSTS':
      return []
    default:
      return oldPosts
  }
}

export function getFiltered(posts, filtered) {
  if (!filtered || filtered <= 0) {
    return posts
  }
  return posts.filter(({ userId }) => userId == filtered)
}

export function addPosts(addPosted) {
  return {
    type: 'ADD_POSTS',
    payload: addPosted
  }
}

export function removePost() {
  return {
    type: 'REMOVE_POSTS'
  }
}

export function addPost(newPost) {
  console.log('addPost')
  return {
    type: 'ADD_POSTS',
    payload: [ newPost ]
  }
}
