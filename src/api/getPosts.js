export function getPosts(connectClient) {
  return new Promise((resolve, reject) => {
    console.log('posts emmit')
    connectClient.emit('posts', (error, posts) => {
      if (error != null) {
        reject(error)
        return
      }
      console.log(posts)
      resolve(posts)
    })
  })
}
